import { chords, parseToSh, Chord, transposeChord } from '~/utils/chords';
import { stringToTabData } from '~/utils/tabs';
import { getEntity, getEntityIndex } from '~/utils/any';
import { Entity, Data, SongObj, Comment, TabData } from '~/classes/interfaces';

/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */

export const state = () => ({
  state: {
    song: {
      performer: {},
      user: {},
      strumming: [],
    },
    comments: [],
    uniqueChords: [],
    tabs: [],
    tonality: 0,
    instrument: 'guitar',
  },
});

export const mutations = {
  setSong(state, songBase: SongObj | Entity | null) {
    if (songBase) {
      state.song = songBase;
      state.song.user = songBase.user_added;
    }
  },

  parseChords(state) {
    if (typeof state.song?.chords === 'string') {
      try {
        state.song.chords = JSON.parse(state.song?.chords);
      } catch {
        state.song.chords = {};
      }
    }
  },

  setChords(state, v) {
    if (typeof v === 'object') {
      state.song.chords = v;
    }
  },

  setUniqueChords(state) {
    if (state.song.chords) {
      let allChords: string[] = [];
      for (const key in state.song.chords) {
        allChords = allChords.concat(state.song.chords[key]);
      }
      const unique = [...new Set(allChords)];

      const result: Chord[] = [];
      for (const uniqueChord of unique) {
        for (const chord of chords) {
          if (parseToSh(uniqueChord) === chord.name) {
            result.push(chord);
          }
        }
      }

      state.uniqueChords = result;
    }
  },

  setTabsOfSong(state) {
    if (state.song.tabs) {
      try {
        const tabs: string[] = JSON.parse(state.song?.tabs);
        const result: TabData[] = [];

        for (const tab of tabs) {
          result.push(stringToTabData(tab));
        }

        state.tabs = result;
      } catch {
        state.tabs = [];
      }
    }
  },

  parseStrumming(state) {
    if (
      typeof state.song?.strumming === 'string' &&
      state.song?.strumming.length
    ) {
      state.song.strumming = state.song?.strumming.split(' ');
    }
  },

  setSongPerformer(state, performer: Entity | null) {
    if (state.song && state.song.performer !== undefined && performer) {
      state.song.performer = performer;
    }
  },

  setSongUser(state, user: Entity | null) {
    if (state.song && state.song.user !== undefined && user) {
      state.song.user = user;
    }
  },

  setComments(state, value: Comment[]) {
    state.comments = value;
  },

  pushComment(state, comment: Comment | null) {
    if (comment) {
      state.comments.push(comment);
    }
  },

  toggleLike(state) {
    if (state.song.is_liked) {
      state.song.is_liked = false;
      state.song.likes_count -= 1;
    } else {
      state.song.is_liked = true;
      state.song.likes_count += 1;
    }
  },

  toggleLikeComment(state, index: number) {
    if (state.comments[index].is_liked) {
      state.comments[index].is_liked = false;
      state.comments[index].likes_count -= 1;
    } else {
      state.comments[index].is_liked = true;
      state.comments[index].likes_count += 1;
    }
  },

  setInstrument(state, v: string) {
    state.instrument = v;
  },
};

export const actions = {
  async loadSong(context, id: number) {
    try {
      const data = await this.$axios.$get(
        `songs/${id}?include[]=user_added.*&include[]=performer.*&`,
      );

      if (!data) {
        return;
      }

      data.song.text = data.song.text.trim();
      context.commit('setSong', data.song);

      context.commit('parseChords');
      context.commit('setUniqueChords');
      context.commit('setTabsOfSong');
      context.commit('parseStrumming');

      context.commit(
        'setSongPerformer',
        getEntity(data.performers, context.state.song?.performer),
      );

      context.commit(
        'setSongUser',
        getEntity(data.users, context.state.song.user_added),
      );
    } catch (e) {
      this.$sentry.captureException(e);
    }
  },

  changeTonality(context, value: number) {
    try {
      context.state.tonality = value;

      const result = {};
      for (const key in context.state.song.chords) {
        const arrayOfChords: string[] = context.state.song.chords[key].slice();
        for (let i = 0; i < arrayOfChords.length; i++) {
          arrayOfChords[i] = transposeChord(arrayOfChords[i], value);
        }
        result[key] = arrayOfChords;
      }

      context.commit('setChords', result);
      context.commit('setUniqueChords');
    } catch (e) {
      this.$sentry.captureException(e);
    }
  },

  async loadComments(context, id?: number) {
    try {
      let url: string = `comments?filter{song}=${id}&include[]=user_added.*`;
      if (typeof id !== 'number') {
        url = `comments?include[]=user_added.*`;
      }
      const data: Data = await this.$axios.$get(url);

      context.commit('setComments', []);

      if (!data.results?.comments.length) {
        return;
      }

      for (const comment of data.results?.comments) {
        comment.user = getEntity(data?.results?.users, comment?.user_added);
        comment.dateAdded = new Date(comment?.date_added);

        context.commit('pushComment', comment);
      }
    } catch (e) {
      this.$sentry.captureException(e);
    }
  },

  async deleteSong(_context, id: number) {
    try {
      await this.$axios.$delete(`songs/${id}`);
    } catch (e) {
      this.$sentry.captureException(e);
    }
  },

  async deleteComment(_context, id: number) {
    try {
      await this.$axios.$delete(`comments/${id}`);
    } catch (e) {
      this.$sentry.captureException(e);
    }
  },

  async like(context) {
    try {
      await this.$axios.$get(`songs/${context.state.song.id}/toggleLike`);
      context.commit('toggleLike');
    } catch (e) {
      this.$sentry.captureException(e);
    }
  },

  async likeComment(context, id: number) {
    try {
      const index: number = getEntityIndex(context.state.comments, id);

      await this.$axios.$get(`comments/${id}/toggleLike`);
      context.commit('toggleLikeComment', index);
    } catch (e) {
      this.$sentry.captureException(e);
    }
  },

  async sendComment(context, text: string) {
    try {
      await this.$axios.$post('comments', {
        text,
        song: context.state.song.id,
      });
      await context.dispatch('loadComments', context.state.song.id);
    } catch (e) {
      this.$sentry.captureException(e);
    }
  },

  async verificate(_context, id: number) {
    try {
      const formData = new FormData();
      formData.append('is_verificated', 'true');
      await this.$axios.$patch(`admin/songs/${id}`, formData);
    } catch (e) {
      this.$sentry.captureException(e);
    }
  },

  async unVerificate(_context, id: number) {
    try {
      const formData = new FormData();
      formData.append('is_verificated', 'false');
      await this.$axios.$patch(`admin/songs/${id}`, formData);
    } catch (e) {
      this.$sentry.captureException(e);
    }
  },
};
